import configparser
import csv
import os

import mysql.connector as mysql
from mysql.connector.errors import IntegrityError

from artcrawler.crawler_utils import read_crawler_json

THIS_DIR = os.path.dirname(os.path.realpath(__file__))

config = configparser.ConfigParser()
db_params = config.read(os.path.join(THIS_DIR, 'config.cfg'))

host = config.get('database', 'host')
user = config.get('database', 'user')
passwd = config.get('database', 'passwd')

db = mysql.connect(
    host=host,
    user=user,
    passwd=passwd,
)

cursor = db.cursor()
try:
    # cursor.execute("DROP DATABASE search_results")
    cursor.execute("CREATE DATABASE search_results")
    cursor.execute("USE search_results")
    cursor.execute("CREATE TABLE IF NOT EXISTS  results "
                   "("
                   "id MEDIUMINT NOT NULL AUTO_INCREMENT,"
                   "url VARCHAR(512) UNIQUE ,"
                   "title TEXT CHARACTER SET utf8,"
                   "keyword TEXT CHARACTER SET utf8,"
                   "date varchar(50),"
                   "text MEDIUMTEXT CHARACTER SET utf8,"
                   "money MEDIUMTEXT CHARACTER SET utf8,"
                   "notes MEDIUMTEXT CHARACTER SET utf8,"
                   "PRIMARY KEY (id, url)"
                   ")")
except:
    print('Database already exists. No need to create it.')
    pass


def write_to_db(crawler_ouput_file):
    print(crawler_ouput_file)
    cursor.execute("USE search_results")
    cursor.execute("CREATE TABLE IF NOT EXISTS  results "
                   "("
                   "id MEDIUMINT NOT NULL AUTO_INCREMENT,"
                   "url VARCHAR(512) UNIQUE ,"
                   "title TEXT CHARACTER SET utf8,"
                   "keyword TEXT CHARACTER SET utf8,"
                   "date varchar(50),"
                   "text MEDIUMTEXT CHARACTER SET utf8,"
                   "money MEDIUMTEXT CHARACTER SET utf8,"
                   "notes MEDIUMTEXT CHARACTER SET utf8,"
                   "PRIMARY KEY (id, url)"
                   ")")
    try:
        for result in read_crawler_json(crawler_ouput_file):
            print(result)
            url = result.url
            keyword = ' '.join([i for i in result.query.split() if 'site:' not in i])
            title = result.title
            date = result.date
            text = result.text
            if isinstance(text, list):
                text = ';'.join(text)
            money = ';'.join(result.money)
            try:
                cursor.execute(
                    """INSERT INTO results (url, title, keyword, date, text, money) VALUES(%s, %s, %s, %s, %s, %s)""",
                    (url, title, keyword, date, text, money))
                db.commit()
            except IntegrityError:
                # raise
                cursor.execute('SELECT keyword FROM results WHERE url = %s', (url,))
                for k in cursor:
                    if k:
                        kws = set(k[0].split())
                        current_keywords = set(keyword.split())
                        if not current_keywords.issuperset(kws):
                            kws.update(current_keywords)
                            new_kw = ' '.join(sorted(list(kws))).strip()
                            cursor.execute(
                                f'UPDATE results SET keyword = "{new_kw}" WHERE url = "{url}"'
                            )
    except:
        pass
    finally:
        pass
        # cursor.close()


def write_to_csv(output_file):
    # rows = conn.query('SELECT * FROM results')
    cursor.execute("USE search_results")
    cursor.execute('SELECT * FROM results')
    columns = [col[0] for col in cursor.description]
    with open(output_file, 'w') as fout:
        writer = csv.writer(fout)
        writer.writerow(columns)
        for row in cursor:
            writer.writerow(row)
        # fout.write(rows.export('csv'))
    # conn.close()


THIS_DIR = os.path.dirname(os.path.realpath(__file__))
CRAWLER_OUTPUT = os.path.join(THIS_DIR, '..', 'crawler', 'output')

if __name__ == '__main__':
    # for f in os.listdir(CRAWLER_OUTPUT):
    #     print(f'Processing the results from {f}')
    #     write_to_db(os.path.join(CRAWLER_OUTPUT, f))
    write_to_csv('results.csv')
