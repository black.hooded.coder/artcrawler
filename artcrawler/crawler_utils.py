import json
import re
import unicodedata
from dataclasses import dataclass
from datetime import datetime, timedelta

from requests_html import HTMLSession

session = HTMLSession()

DATE_FORMAT = '%b %d, %Y'
DATE_FORMAT_ALT = '%Y-%m-%dT%H:%M:%S'  # 2019-03-22T00:00:00
RELATIVE_DATE = re.compile(r'^(([0-9]+)([dhm]|mon)|(\d+) (hour|minute|day|week)s? ago)$')  # 1d, 2h, 4w, etc.


@dataclass
class QueryResult:
    url: str
    title: str
    snippet: str
    date: str
    query: str
    text: list = ()
    money: list = ()

    def __eq__(self, other):
        return self.url == other.url

    def __hash__(self):
        return hash(self.url)


def get_full_title(result):
    if result.get('title').endswith(' ...'):
        res = session.get(result.get('url'))
        try:
            t = res.html.find('title', first=True).text
            if t:
                result['title'] = t
        except AttributeError:
            raise
    return result


def get_text_snippets(result):
    q = result.get('query')
    q = q.split(' site:')[0].strip()
    res = session.get(result.get('url'))
    q = f' {q} '
    text = [i.text for i in res.html.find('p', containing=q) if i.text][:2]
    money = [i.text for i in res.html.find('p', containing='$') if i.text][:2]
    money += [i.text for i in res.html.find('p', containing='£') if i.text][:2]
    money += [i.text for i in res.html.find('p', containing='€') if i.text][:2]
    if text:
        result['text'] = text
    else:
        result['text'] = result.get('snippet')
    result['money'] = money or []
    return result


def _time_delta(amount, unit):
    amount = int(amount)
    if unit == 'h' or unit == 'hour':
        return datetime.now() - timedelta(hours=amount)
    elif unit == 'd' or unit == 'day':
        return datetime.now() - timedelta(days=amount)
    elif unit == 'm' or unit == 'minute':
        return datetime.now()
    elif unit == 'mon' or unit == 'month':
        return datetime.now() - timedelta(weeks=(amount * 4))
    else:
        return datetime.now()


def format_date(result):
    try:
        d = result.get('date')
        is_relative = RELATIVE_DATE.match(d)
        if is_relative:
            amount = is_relative.group(2) or is_relative.group(4)
            unit = is_relative.group(3) or is_relative.group(5)
            date = _time_delta(amount, unit).isoformat()
        else:
            date = datetime.strptime(result.get('date')
                                     .replace('.', ''), DATE_FORMAT).isoformat()
    except:
        date = result.get('date')
        pass
    return date


def read_crawler_json(filepath):
    with open(filepath) as fin:
        json_data = json.load(fin)
        for query, pages in json_data.items():
            for page_number, result_data in pages.items():
                results = result_data.get('results')
                for result in results:
                    title = unicodedata.normalize("NFKD", result.get('title'))
                    snippet = unicodedata.normalize("NFKD", result.get('snippet'))
                    date = format_date(result)
                    yield QueryResult(**get_text_snippets(get_full_title(
                        {
                            'url': result.get('link'),
                            'title': title,
                            'snippet': snippet,
                            'date': date,
                            'query': query,
                        }
                    )))
