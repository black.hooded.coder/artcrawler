#!/usr/bin/env python
import os


from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))


packages = ['artcrawler']

setup(
    name='artcrawler',
    version='1.0.0',
    description='',
    long_description='',
    author='Jay Rod',
    packages=packages,
    include_package_data=True,
    python_requires=">=3.7.*",
)