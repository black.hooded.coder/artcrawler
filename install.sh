#!/usr/bin/env bash

# Install Node and NPM

# https://docs.npmjs.com/downloading-and-installing-node-js-and-npm
# https://nodejs.org/en/download/

# Run npm install
cd crawler && npm install && cd ..

# Install pip, venv, requirements, etc.

curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3 get-pip.py

rm get-pip.py

python3 -m venv ./venv/
source ./venv/bin/activate

pip3 install --upgrade pip
pip3 install -r requirements.txt

python3 setup.py install
