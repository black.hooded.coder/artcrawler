import os

from artcrawler.db_utils import write_to_db

THIS_DIR = os.path.dirname(os.path.realpath(__file__))
CRAWLER_OUTPUT = os.path.join(THIS_DIR, 'crawler', 'output')

if __name__ == '__main__':
    for f in os.listdir(CRAWLER_OUTPUT):
        fpath = os.path.join(CRAWLER_OUTPUT, f)
        print(f'Processing the results from {fpath}')
        write_to_db(fpath)
        os.remove(fpath)
    print('Done!')