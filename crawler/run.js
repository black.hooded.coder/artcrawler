const fs = require('fs');
const se_scraper = require("se-scraper");

const Cluster = {
    CONCURRENCY_PAGE: 1, // shares cookies, etc.
    CONCURRENCY_CONTEXT: 2, // no cookie sharing (uses contexts)
    CONCURRENCY_BROWSER: 3, // no cookie sharing and individual processes (uses contexts)
};

const f = (a, b) => [].concat(...a.map(d => b.map(e => [].concat(d, e))));
const cartesian = (a, b, ...c) => (b ? cartesian(f(a, b), ...c) : a);

const getParams = () => {
    try {
        const content = fs.readFileSync('./params.json');
        const data = JSON.parse(content);
        return data;
    } catch (err) {
        console.error(err)
    }
};

const getQueries = () => {
    try {
        const {includeKeywordOnly, limitSite, seeds, keywords} = getParams();
        if (!limitSite) {
            const queries = keywords.map((kw) => `${kw.replace(/, /g, ' ')}`);
            return queries
        } else {
            const prods = cartesian(keywords, seeds);
            const queries = prods.map((i) => `${i[0].replace(/, /g, ' ')} site:${i[1]}`);
            if (includeKeywordOnly) {
                queries.push(keywords);
            }
            return queries
        }
    } catch (err) {
        console.error(err)
    }
};


const multiple_search_engines = async () => {
    const {searchEngines, sleepBetweenSearches, useProxies} = getParams();
    const queries = getQueries();
    for (const engine of searchEngines) {
        console.log(`** SEARCHING ON ${engine}... **`);
        const config = {
            search_engine: engine,
            sleep_range: sleepBetweenSearches,
            random_user_agent: true,
            debug: true,
            verbose: true,
            keywords: queries,
            num_pages: 1,
            block_assets: true,
            output_file: `./output/${engine}.json`,
            proxy_file: useProxies ? 'proxies.txt' : '',
            log_ip_address: true,
            puppeteer_cluster_config: {
                timeout: 30 * 60 * 1000, // max timeout set to 30 minutes
                monitor: false,
                concurrency: Cluster.CONCURRENCY_PAGE, // one scraper per tab
                maxConcurrency: 1, // scrape with 2 tabs
            }
        };
        await se_scraper.scrape(config, (err, response) => {
            if (err) {
                console.error(err)
            }
            console.dir(response, {
                depth: null,
                colors: true
            });

        });
    }
};

multiple_search_engines();
