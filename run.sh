#!/usr/bin/env bash

# Run the JS crawler
cd crawler && node run.js && cd ..

# Install pip, venv, requirements, etc.

python3 -m venv ./venv/
source ./venv/bin/activate

# run Python main.py
python3 main.py
