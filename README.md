# Keyword Search Engine Scraper


This node module allows you to scrape search engines concurrently with different proxies.


##### Table of Contents
- [Installation](#installation)
- [Quickstart](#quickstart)
- [Using Proxies](#proxies)


Se-scraper supports the following search engines:
* google
* google_news_old
* google_news (App version) (https://news.google.com)
* google_image
* amazon
* bing
* bing_news
* baidu
* infospace
* duckduckgo
* webcrawler
* reuters
* cnbc
* marketwatch


## Installation

* You need a working installation of **node** and the **npm** package manager.
* You also need `Python 3.7` installed

To install/update to `Python 3.7` on a Debian based system you need to run the following commands:

```bash
sudo apt update
sudo apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget
cd /tmp
wget https://www.python.org/ftp/python/3.7.2/Python-3.7.2.tar.xz
tar -xf Python-3.7.2.tar.xz
cd Python-3.7.2
./configure --enable-optimizations
make -j 4 (this can take a while)
sudo make install
```
You might also need to install:
```bash
sudo apt-get install libmysqlclient-dev
```
To run the installation script, change directory (cd) into this project folder and do:

```bash
. ./install.sh
```

For Debian based systems, you might need to install some dependencies. Just run the following command:
```bash
apt-get install -y wget unzip fontconfig locales gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget
```

## Quickstart

### MySQL Database
Edit the `config.cfg` file that is under `artcrawler/artcrawler` with your corresponding configuration.
Here is an example of how it might look like:
```editorconfig
[database]
host = localhost
user = root
passwd = myp4ssw0rd
```
### Edit this [`params.json` file](https://gitlab.com/black.hooded.coder/artcrawler/blob/master/crawler/params.json) to
adjust the search accordingly to your preferences.

A few examples of parameters that you can customize are:

* Pause in seconds between searches.
```json
"sleepBetweenSearches": [2, 5],
```

* Additionally to including a search query like `keyword site:somedomain.com`,
When this parameter set to `true`, the search will also make a query containing only the keyword.
Setting this to `true`, might make the search less precise.

```json
"includeKeywordOnly": false,
```

* Setting this to `true` causes the crawler to limit the search to websites listed
in the `seeds` list.

```json
"limitSite": false,
```

* Use proxies (add valid proxy addresses to the file `crawler/proxies.txt`. This increases memory consumption of the
crawler)
```json
"useProxies": false,
```

* Use seed domains to limit search and get more precise results
```json
  "seeds": [
    "afr.com/lifestyle/arts-and-entertainment/art",
    "antiquesandthearts.com",
    "antiquestradegazette.com",
    "artdaily.com",
    "artforum.com/news"
  ],
```

* Keywords to be searched
```json
 "keywords": [
    "china, market",
    "collector, art lending",
    "collector, dealer, wwii, lawsuit",
    "collector, lawsuit",
    "copyright, lawsuit, artist"
  ]
```
### Start scraping by firing up the command `. ./run.sh`

The output will be saved to a database in `artcrawler/results.db`.

## Proxies

**se-scraper** will create one browser instance per proxy. So the maximal amount of concurrency is equivalent to the number of proxies plus one (your own IP).


With a proxy file such as

```text
53.34.23.55:55523
51.11.23.22:22222
```

This will scrape with **three** browser instance each having their own IP address. Unfortunately, it is currently not possible to scrape with different proxies per tab. Chromium does not support that.
